package com.xsm.library;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * 文本先关工具
 */
public class TEditTools {

    /**
     * 只能输入中文数字字母下划线
     *
     * @param string 内容
     * @return 返回替换后的内容
     * @throws PatternSyntaxException 异常
     */
    public static String stringFilter(String string) throws PatternSyntaxException {
        String regEx = "[^a-zA-Z0-9\u4E00-\u9FA5_]";
        Pattern p = Pattern.compile(regEx);
        Matcher m = p.matcher(string);
        return m.replaceAll("").trim();
    }
}
